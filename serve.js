// Use:
// node serve 80(optional) /src(optional)

// Dependencies
const http = require("http");
const url  = require("url");
const path = require("path");
const fs   = require("fs");

function serve (host, port, base, page) {
    http.createServer(function createServer (request, response) {

        const uri = base + url.parse(request.url).pathname;
        let filename = path.join(process.cwd(), uri);

        function fileRead (exists) {
            if(!exists) {
                errorHandler("Not Found", 404)
                return;
            }

            if (fs.statSync(filename).isDirectory()) {
                filename += `/${page}`;
            }

            fs.readFile(filename, "binary", respond);
        }

        function respond (error, file) {
            if (error) {
                errorHandler(error, 500)
                return;
            }

            response.writeHead(200);
            response.write(file, "binary");
            response.end();
        }

        function errorHandler (error, code) {
            response.writeHead(code, {"Content-Type": "text/plain"});
            response.write(code + " \n" + error + " \n");
            response.end();
            return;
        };

        fs.exists(filename, fileRead);

    }).listen(parseInt(port, 10), host);

    console.log(
`Static file server running:
http://${host}:${port}
CTRL + C to shutdown`);

};


/////////////////////////////////
// Autoplay the serve function //
/////////////////////////////////
(function autoplay (argv) {
    // Defaults
    const page = "index.html";
    const host = "127.0.0.1";
    let base = "";
    let port = 1337;

    argv.shift(); // program
    argv.shift(); // file

    if (argv.length > 0) {
        port = parseInt(argv.shift(), 10);
    }

    if (argv.length > 0) {
        base = argv.shift();
    }

    base = `${base}/`.replace(/\/\/*/g, '/'); // Remove repeating backslashes

    serve(host, port, base, page);
}([].slice.call(process.argv)));
