# Reactenv
### A basic React and redux setup, with SASS interpolation and jQuery for the meek.

Has a watch job for live build, but page still needs to be refreshed

## Set up
```sh
npm install
```

## Run
```sh
npm start
```

## Open
open `http://127.0.0.1:1337` on your browser

## Edit
| Entry | File |
| ----- | ---- |
| HTML | `/index.html` |
| JS | `/lib/js/index.js` |
| CSS | `/lib/scss/index.scss` |

> Note, SASS compiler only runs as watch (after source file changes), so if you're running it and nothing happens, just add a new line at the source file or something
