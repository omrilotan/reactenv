const path = require('path');
const webpack = require('webpack');

const ROOT = path.resolve(__dirname);

module.exports = {
    context: __dirname,
    stats: {
        warnings: false,
        reasons: false
    },
    entry: {
        "index": `${ROOT}/lib/js/index.js`
    },
    output: {
        path: `${ROOT}/public/js/`,
        filename: '[name].js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: [
                        'es2015',
                        'react'
                    ]
                }
            }
        ]
    },
    externals: {
        "jquery": "jQuery",
        "react" : "React",
        "react-dom" : "ReactDOM",
        "react-redux" : "ReactRedux",
        "redux" : "Redux"
    },
    devtool: 'inline-source-map'
};
