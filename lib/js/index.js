import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import $ from 'jquery';

document.getElementById('test').appendChild(document.createTextNode('Hello, world!'));

console.info('JavaScript code goes here', { React, ReactDOM, Provider, createStore, $ });
